# Tutorial MQTT

## Introdução a MQTT

O [MQTT](http://mqtt.org/) (Message Queuing Telemetry Transport) é um protocolo de mensagens leve otimizado para redes TCP/IP. É baseado na arquitetura de comunicação indireta Pub/Sub. Este protocolo foi desenvolvido pela IBM e pela Eurotech e tem como finalidade comunicar dados através de redes com pouca largura de banda, com muita latência e, nesse sentido, pouco confiáveis. O exemplo mais comum de uso é em IoT, onde lidamos com dispositivos limitados, com poucos recursos, como sensores.

### Arquitetura
![arquitetura MQTT](https://pplware.sapo.pt/wp-content/uploads/2019/03/MQTT_01-720x425.png)
A arquitetura consiste 3 componentes essenciais:
- **publisher**: é aquele que se liga ao broker e publica as mensagens.
- **broker**: middleware responsável por receber e disparar as mensagens recebidas dos publishers para os subscribers.
- **subscribers**: se liga ao broker e recebe as mensagens que ele estiver interesse (inscrito).

## Vantagens do uso
- Por ter uma **facilidade de implementação** permite que o protocolo funcione mesmo em sistemas poucos modernos ou até mesmo com internet limitada;
- O broker garante **desacoplamento entre as partes comunicantes**, logo, publishers e subscribers não precisam ter relação direta;
- Com o broker é possível rodar sobre SSL e autenticação para aumentar a **segurança**.
- Somente o necessário passa pelo protocolo MQTT o que faz com que ele **não fique sobrecarregado**.

## Broker
O Broker é uma espécie de middleware responsável por receber e disparar as mensagens recebidas dos publishers para os subscribers. Ou seja, a troca de mensagens realizado pelo protocolo MQTT é centralizado no broker, que realiza a mediação da comunicação. Entretanto, é possível implementar formas de escalar o broker com clusters e outras ferramentas que possibilitam maior escalabilidade e disponibilidade.

Neste exemplo, utilizamos o brooker [CloudMQTT](https://www.cloudmqtt.com/docs/index.html). Optamos por esta ferramenta por ser bastante simples de configurar e possui suporte a WebSockets.

- Crie sua conta no CloudMQTT, acessando o console: [CloudMQTT Console](https://customer.cloudmqtt.com/instance);
- Após criado, iremos criar uma nova instância;
- Com isso, iremos obter os dados necessário para conexão ao nosso broker, minimamente, são eles:
	- URL do host
	- porta
	- nome do usuário
	- senha

Existem vários outros brokers MQTT disponíveis. Alguns são pagos, mas também há uns gratuitos para testes.

## MQTT Linha de Comando

O protocolo MQTT, pode ser utilizado via linha de comando. Para isso, você deve instalar o MQTT globalmente:

```
npm install mqtt -g
```

Após instalado, podemos utilizar os comandos do subscriber e publisher para testarmos o protocolo. Abaixo segue o código do subscriber e publisher, respectivamente. Utilizamos um broker qualquer para fins de testes.

```
mqtt sub -t 'hello' -h 'test.mosquitto.org' -v
```

```
mqtt pub -t 'hello' -h 'test.mosquitto.org' -m 'from MQTT.js'
```

Para fins deste tutorial, focamos na aplicação do protocolo MQTT com NodeJS.

## Instalação MQTT para NodeJS
Para instalar o protocolo execute o comando:

````
npm install mqtt --save
````

## MQTT com NodeJS
Implementaremos uma aplicação que simula um painel de notificação ao seus clientes. Portanto, temos um painel  que enviará mensagens, via requisição HTTP GET, para nosso server. Assim que recebido a requisição, nosso server irá publicar a mensagem para o broker, no qual encaminhará para todos subscribers inscritos no tópico em questão.

Neste exemplo, implementamos as funções do MQTT:
- mqtt.connect()
- mqtt.Client#publish()
- mqtt.Client#subscribe()
- mqtt.Client#end()

Estes comandos possibilitam a conexão com o broker, publicação e recebimento de mensagens. Existem outros comandos que podem ser implementados de acordo com a necessidade. 

Importante ressaltar que, maioria dos métodos disponíveis no protocolo MQTT são executados com parâmetros padrões, mas podem ser alterados através do parâmetro [options], como no método publish, você pode definir o QoS level para publicação, o valor default é 0. Exemplo:

```
client.publish('topic' , 'message', {qos: 2})
```

Um outro exemplo seria a definição dos parâmetros de conexão com o broker.

```
var options = {
	host: 'tailor.cloudmqtt.com',
	port: 13901,
	username: 'username',
  	password: 'password'
};

var client  = mqtt.connect(options)
```

Outra questão importante sobre os métodos do protocolo MQTT, é que os métodos aceitam funções de callback - uma chama de código assíncrona não bloqueante, como demonstrado abaixo.
```
client.publish('topic' , 'message', [options], function () {
	console.log("Message send!")
})
```

O MQTT possui suporte a criação de eventos listeners, sendo assim, eventos podem ser programador para serem executados automáticamente quando o MQTT realizar uma operação. Exemplo: a disparação uma ação assim que que for realizado a conexão com o broker. Portanto, este protocolo é bastante flexível e possui diversos recursos que podemos aplicar conforme o contexto do ambiente. Demonstração:
```
client.on('connect', function () {
	console.log("Connected!")
})
```

Mesmo quando comparado com outros protocolos mais robustos, o MQTT se destaca por ser um protocolo extremamente simples que se adapta facilmente ao ambiente que contêm poucos recursos a disposição.

Segue abaixo código dos scripts criados para exemplicar o uso prático do MQTT com NodeJS.

Código server:
```
var  mqtt  =  require('mqtt')
const  express  =  require('express')
const  app  =  express()
const  port  =  3000

var  mqtthost  =  'tailor.cloudmqtt.com';//URL broker
var  options  = {
	host:  mqtthost,
	port:  13901,
	username:  'ekuoclbn',
	password:  'lIR1oNAfNTVl'
};

var  publisher  =  mqtt.connect(options)//establish connection broker

app.get('/', (req, res) => {
	res.sendFile(__dirname+'/painel.html')
})

//receive topico and msg to send to all subscribers
app.get('/mensagem/:topico&:msg', (req, res) => {
	//params
	var  topico  =  req.params.topico;
	var  msg  =  req.params.msg;
	console.log("DEBUG: SEND > "  +  topico  +  " > "  +  msg)//debug
	
	publisher.publish(topico , msg)//send to subscribers
	
	var  response  = {"topico":  topico, "msg":  msg, "status":  "Enviado" }
	res.send(response)//return response to painel
})

app.listen(port, () =>  console.log(`Servidor executando na porta ${port}!`))
```
Código subscriber:
```
var  mqtt  =  require('mqtt')
var  mqtthost  =  'tailor.cloudmqtt.com';//URL Host
var  options  = {
	host:  mqtthost,
	port:  13901,
	username:  'ekuoclbn',
	password:  'lIR1oNAfNTVl'
};

var  client  =  mqtt.connect(options)//establish connection broker

const  topico  =  "topico1";//topic that will be subscribed
client.on('connect', function () {//on establish connection with broker subscribe to the topic
	client.subscribe(topico, function (err) {
		if (err) {
			console.log("ERRO: "  +  !err)
		}else{
			console.log("Client inscrito no tópico: "  +  topico)
		}
	})
})

// message received
client.on('message', function (topic, message) {
	console.log(message.toString())
})
```

Código do painel de mensagens (HTML)
```
<html>
	<head>
		<meta  charset="utf-8">
		<title>PAINEL DE MSG</title>
		<script  src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<style  type="text/css">
			body,td,th {
				font-family: Arial, Helvetica, sans-serif;
				font-size: 14px; }
			body {
				margin-left: 0px;
				margin-top: 0px;
				margin-right: 0px;
				margin-bottom: 0px; }
			#msg_enviadas  td, #msg_enviadas  th {
				border: 1px  solid  #ddd;
				padding: 8px; }
			#msg_enviadas  th {
				padding-top: 12px;
				padding-bottom: 12px;
				text-align: left;
				background-color: #4CAF50;
				color: white;
				text-align: center;	}
		</style>
	</head>
	<body>
		<div  style="background:#06C; color:#FFF; padding:10px; font-size:24px; text-align:center; margin-bottom:50px;">PAINEL DE MENSAGENS</div>
		<div  style="padding:5px;">
			<br />
			<table  width="723"  border="0"  align="center"  cellpadding="5"  cellspacing="0"  style="border:#000 1px solid;">
				<tr>
					<td  colspan="2"  bgcolor="#069"  style="color:#FFF; text-align:center; font-size:16px; padding:10px;">ENVIAR MENSAGEM</td>
				</tr>
				<tr>
					<td  colspan="2"  bgcolor="#000000"  style="color:#FFF; text-align:center; font-size:12px;">Enviar mensagem para subscribers</td>
				</tr>
				<tr>
					<td  width="215">Tópico</td>
					<td  width="486">
						<select  id="topico"  value=""  style="width:98%;">
							<option  value="topico1">Topico 1</option>
							<option  value="topico2">Topico 2</option>
							<option  value="topico3">Topico 3</option>
							<option  value="topico4">Topico 4</option>
						</select>
					</td>
				</tr>
				<tr>
					<td  width="215">Mensagem</td>
					<td  width="486"><input  id="msg"  type="text"  value=""  style="width:98%;" /></td>
				</tr>
				<tr>
					<td  height="53">&nbsp;</td>
					<td><input  type="button"  value="ENVIAR"  style="font-size:18px;"  onclick="enviar();"/></td>
				</tr>
			</table>
			<br><br><br>
			<div  style="display:flex; justify-content:center; flex-wrap: wrap; margin:-1rem;">
				<table  style="border:1px solid #000000; border-collapse: collapse; margin: 0; justify-items:center; width:723px"  id="msg_enviadas">
					<thead>
						<tr>
							<th>Tópico</th>
							<th>Mensgem</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
		<script>
			function  enviar(){
				var  topico  =  $("#topico").val();
				var  msg  =  $("#msg").val();
				$.get("/mensagem/"+topico+"&"+msg, function(data, status){
					$('#msg_enviadas tr:last').after('<tr><td>'+data.topico+'</td><td>'+data.msg+'</td><td>'+data.status+'</td></tr>');
				});
			}
		</script>
	</body>
</html>
```

### Execução
Painel de envio de mensagens:
![Painel de mensagens](https://bcmlab.com.br/SD/gifs/painel.gif)

O server, por sua vez, ao receber a mensagem realiza um debug no console e publica a mensagem no broker.
![Server](https://bcmlab.com.br/SD/gifs/server.gif)

O subscriber possui um listener que ao identificar novas mensagens imprime-as no console.
![Client](https://bcmlab.com.br/SD/client.gif)

## Alerta Sobre Risco de Segurança
Apesar de bastante eficiente, alguns cuidados quanto a implementação do MQTT devem ser cuidados. O artigo [Attack Scenarios and Security Analysis of MQTT Communication Protocol in IoT System](https://www.researchgate.net/publication/322059897_Attack_scenarios_and_security_analysis_of_MQTT_communication_protocol_in_IoT_system) aborda questões importantes sobre a não implementação do mínimo de segurança. Como sabemos, o MQTT é um protocolo leve voltado para contextos com poucos recursos. Levando isto em consideração, o artigo levanta questões importantes referente a segurança:

- Dispositivo com restrição de recursos: dispositivo não consegue lidar com a maioria das abordagens de segurança;
- Grande número de dispositivos: a grande de diversidade de dispositivos conectados pode criar vulnerabilidades, uma vez que, é dificil gerenciar muitos tipos de dispositivos diferentes;
- Falta de segurança: a não conscientização da segurança como um fator primordial, faz com que o desenvolvedor dê preferência para requisitos que favorecem a funcionalidade.

Toda comunicação do MQTT é centralizado em um broker, logo, se não há nenhum requisito mínimo de segurança na comunicação, teremos uma falha de segurança grandes podendo sofrer ataques que prejudiquem os dados. Portanto, imaginando uma implementação do MQTT em sensores para captção de dados, é fácil concluirmos que uma mínima segurança é extremamente importante para que os subscribers tenha confiança nos dados recebidos.

Neste artigo, demonstram um cenário de ataque onde as mensagens que são publicadas no broker, sem nenhum requisito de segurança, podem ser interceptadas. E assim, quem está realizando o ataque pode ter acesso ao seu conteúdo. Além disso, poderá publicar mensagens direcionadas ao seu tópico e assim manipular os subscribers. 

![Attack Scenarios](https://www.researchgate.net/profile/Bagus_Hanindhito/publication/322059897/figure/fig4/AS:621517926826001@1525192829900/Attack-scenario-from-attackers-publisher.png)

Acima podemos ver uma situação onde o MQTT foi implementado para controlar lâmpadas das ruas, logo, havendo uma falha de segurança é possível que outros consigam acesso e manipule os dispositivos.

Observação: mesmo que o MQTT seja implementado apenas em rede local, os requisitos de segurança continuam extremamente importantes porque os pacotes do protocolo transitam através da rede por TCP/IP, por padrão, sem nenhuma criptografia. Ou seja, os pacotes podem ser interceptados e manipulados utilizando ferramentas que fazem monitoramente da rede, como [Wireshark](https://www.wireshark.org/).

Portanto, ao se utilizar o protocolo MQTT deve ser analisado o tipo de dado que será transmitido, e caso sejam dados de certa relevância, devemos ter o cuidado de utilizarmos brokers que oferecem, pelo menos, o mínimo de segurança, como:

- SSL/TSL;
- autenticação de usuário e senha;
- Implementar MQTT em uma porta diferente da porta default (1883);
- Analisar possibilidade de implementar métodos de criptografia nos dados, como [RSA](https://medium.com/@tarcisioma/algoritmo-de-criptografia-assim%C3%A9trica-rsa-c6254a3c7042).

Estas são algumas sugestões de segurança que podem implementadas e assim garantir alguma segurança para sua aplicação, considerando todos fatores já apresentados.

## Conclusão
Podemos concluir que a implementação do MQTT é bastante simples, portanto comprova as vantagens de uso apresentadas. Assim, o MQTT, por ser leve e simples, é um protocolo ótimo para aplicações que possuem recursos limitados. Entretanto, podemos concluir também que os riscos de segurança podem danificar a aplicação e requisitos de segurança devem ser bem analisados, caso necessário, implementados.

## Referências

- [GitHub - MQTTJS](https://github.com/mqttjs/MQTT.js/)
- [CloudMQTT - Broker](https://www.cloudmqtt.com/docs/index.html)
- [Medium - Experimentando a Node MCU com Nodejs e MQTT](https://medium.com/@czarantoniodesouza/experimentando-a-node-mcu-com-nodejs-e-mqtt-798bc5666d2f)
- [RisingStack - Getting started with Node.js with MQTT](https://blog.risingstack.com/getting-started-with-nodejs-and-mqtt/)
- [PPLWare - MQTT: Protocolo de comunicação para pequenos dispositivos móveis](https://pplware.sapo.pt/tutoriais/networking/mqtt-protocolo-de-comunicacao/)
- [EngProcess - O que é MQTT](https://engprocess.com.br/mqtt-broker/)
- [ResearchGate - Artigo: Attack Scenarios and Security Analysis of MQTT Communication Protocol in IoT System](https://www.researchgate.net/publication/322059897_Attack_scenarios_and_security_analysis_of_MQTT_communication_protocol_in_IoT_system)
